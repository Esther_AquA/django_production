#!/usr/bin/env bash

# run the migration and collect the static files
# not sure that need to be done everytime, only if changes where done.
source ~/.virtualenvs/djangotest/bin/activate
python manage.py collectstatic
python manage.py makemigrations
python manage.py migrate