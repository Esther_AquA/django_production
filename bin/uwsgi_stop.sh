#!/usr/bin/env bash

# shut down uwsgi
uwsgi --stop /tmp/microdomains.pid

# gracefully stop nginx
sudo nginx -s quit
echo `date`
