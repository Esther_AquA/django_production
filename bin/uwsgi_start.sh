#!/usr/bin/env bash

# run django_changes.sh before if any changes need to be applied.

# start nginx
sudo nginx

# start uwsgi
uwsgi --ini ./conf/microdomains_uwsgi.ini

