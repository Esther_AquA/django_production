#!/usr/bin/env bash

# start nginx
#sudo nginx

# start gunicorn
echo `date`
gunicorn -c conf/gunicorn.conf.py microdomains.wsgi --daemon