#!/usr/bin/env bash

# shut down gunicorn
echo `date`
pkill gunicorn

# gracefully stop nginx
sudo nginx -s quit