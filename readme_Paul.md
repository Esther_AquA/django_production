### How to set up nginx + uwgsi without Docker

Steps:
 
nginx
- install nginx gobally on the machine by using a package manager. On Mac brew on Linux `apt-get nginx`
- to configure nginx copy the file ```conf/microdomains_nginx.conf``` into your local project, replacing "microdomains" 
with your project name if you like (just need to stay consistent)
- Set a softlink to the configuration file from the basic nginx folder which can be found (on Mac) at : `usr/local/etc/nginx/`. 
This might be slighty different on Linux, just check where this folder is and the ```nginx.conf``` file. 
- Open the ``nginx.conf`` file and check if either `include servers/` (on Mac) or `include enabeled_applications/`is included in the file. 
And if not create that folder wihtin the nginx folder and add the line to the nginx.conf file. 
- Set a softlink from within that folder to your local conf file in your project with ```sudo ln -s path/project_nginx.conf path//usr/local/etc/nginx/servers```. 
Check if the linking was successful. For me this sometimes only worked when I ran the command from the root of the machine, but I don't really know why. 
- create at strucure of log-folder for the log files while running. Struture such as: log/nginx/ and log/uwsgi (and log/gunicorn, if using gunicorn afterall)


uwsgi
- is installed only in the virtual env, not globally. So add it to your local env with ```pip install uwsgi```
- copy `uwsgi_params` and `microdomains_uwsgi.ini` into your local project and change the paths in the ladder file accordingly. 
Unfortunately I haven't gotten around using total paths in these files, so one has to taylor this to the local project. 
Change the path for directory base and where to find the virtual enviroment. 
- create the log folder

starting the servers: 
- copy the scripts in ``bin/``
- run ```django_changes.sh``` is any migration or static file changes, or if you are running this for the first time
- run `uwsgi_start.sh` to start nginx and uwsgi
- to check if nginx is runnig on the right port use `` ps aux | grep nginx``  or ``lsof -nPL -iTCP:80``to specifically 
check which process are running on port 80.
- to grecefully stop run the ```uwsgi_stop.sh``` script for stoppping. 


Trouble shooting: 
- [ERROR](https://stackoverflow.com/questions/59568777/nginx-emerg-bind-to-0-0-0-08080-failed-48-address-already-in-use-on-mac) 
"port already in use": If nginx isn't able to start as another process is already using the port
check which process this is through the command above and change the port of the nginx if
nessercary or kill the process if it's not important (``kill processid`). Some time the pid file might be messed up and need to be deleted:

    `sudo rm /usr/local/var/run/nginx.pid`

- [ERROR](https://stackoverflow.com/questions/20182329/nginx-is-throwing-an-403-forbidden-on-static-files):
Nginx is throwing an 403 Forbidden on Static Files : add user name into the ``nginx.conf`` file


Comments: 
- the ```microdomain_nginx.conf``` is currently set up to work with uwgsi, I also tired with gunicorn, but could only make it work using ports, not sockets. 
To run with gunicorn instead change the two lines in the ```microdomain_nginx.conf``` file which are indicated by the comments as option B. 
For this also copy the gunicorn.conf.py and the scripts to start the servers with gunicorn ```gunicorn_start.sh```