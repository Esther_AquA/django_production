command = '/Users/estherweil/.virtualenvs/djangotest/bin/gunicorn'
pythonpath= '/Users/estherweil/Desktop/product/django_production/microdomains'
#bind='unix:/tmp/microdomains.sock'
bind='127.0.0.1:8000'
workers=3
#loglevel='error'
errorlog='/Users/estherweil/Desktop/product/django_production/microdomains/log/gunicorn/error.log'
accesslog='/Users/estherweil/Desktop/product/django_production/microdomains/log/gunicorn/access.log'
env = ['DJANGO_SETTINGS_MODULE=microdomanins.settings']
#pid='/tmp/microdomains.pid'


#pkill gunicorn